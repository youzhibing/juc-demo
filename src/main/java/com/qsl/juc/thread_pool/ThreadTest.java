package com.qsl.juc.thread_pool;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 当Java的某个线程处于可中断的阻塞状态时，你用另一个线程调用该线程的interrupt()方法时，JVM会使该线程离开阻塞状态，并抛出一个异常。
 * 既然该线程已经离开阻塞状态，自然要参与到对CPU时间的争夺中，当获取到CPU时间时自然可以处理该异常
 * <p>
 * 线程被唤醒一般有两种情况：1、被其他线程唤醒，2、线程自身因为被中断等原因而被唤醒
 * <p>
 * 其他线程可以调用本线程的 interrupt 方法，至于本线程是否相应该中断，以及如何相应该中断，由本线程自己决定
 */
public class ThreadTest {

    public static void main(String[] args) {

        // 查看 cpu 核心数
        System.out.println(Runtime.getRuntime().availableProcessors());

        System.out.println(Thread.activeCount());
    }
}
