package com.qsl.juc.thread_pool;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author 青石路
 * @date 2021/3/17 22:14
 */
public class ThreadPoolTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(3);

        Future future = executorService.submit(() -> {
            System.out.println("submit");
        });
        future.get();

        executorService.execute(() -> {
            System.out.println("execute");
        });

        // SHUTDOWN 状态
        executorService.shutdown();

        // STOP 状态； tasks：等待被执行的任务列表
        List<Runnable> tasks = executorService.shutdownNow();
    }
}
