package com.qsl.juc.volatil1;

/**
 * 伪共享
 * stop 可能与 i 同时在 cpu 的 cache line 中
 * 那么 volatile i 的可见顺带让 stop 也可见了
 * 所以，有些时候，下面的代码能正常退出
 * <p>
 * 但是这不能作为 stop 线程可见性的根据，因为 stop 和 i 很有可能不在同一个 cache line 中
 *
 * @author 青石路
 * @date 2021/2/24 10:29
 */
public class PseudoSharingTest {

    private static boolean stop = false;
    // 将 volatile 拿掉，那后面的代码肯定是不能正常退出的
    private static volatile int i = 0;

    public static void main(String[] args) throws Exception {

        new Thread(() -> {
            while (!stop) {
                i++;
            }
        }).start();

        // 主线程休眠1秒，让子线程启动起来，保证主线程对 stop 的修改后于子线程的运行
        Thread.sleep(1000);

        stop = true;
    }
}
