package com.qsl.juc.volatil1;

import java.util.concurrent.CountDownLatch;

/**
 * volatile变量自增运算测试
 *
 * @author 青石路
 */
public class VolatileTest {
    private static final int THREADS_COUNT = 20;
    private static CountDownLatch latch = new CountDownLatch(THREADS_COUNT);

    // public static AtomicInteger race_ai = new AtomicInteger(0);
    public static int race_int = 0;

    public static void increase() {
        // race_ai.incrementAndGet();
        race_int++;
    }

    public static void main(String[] args) throws Exception {

        for (int i = 0; i < THREADS_COUNT; i++) {
            new Thread(() -> {

                for (int j = 0; j < 10000; j++) {
                    increase();
                }
                latch.countDown();

            }).start();
        }

        latch.await();
        // System.out.println("race_ai = " + race_ai + ", race_int = " + race_int);
        System.out.println("race_int = " + race_int);
    }
}