package com.qsl.juc.volatil1;

import java.util.HashSet;
import java.util.Set;

/**
 * 乱序示例
 * as-if-serial 只是针对单线程
 * 多线程情况太复杂，cpu 无法保证多线程之间的 as-if-serial（或者说，可以保证，但cpu效率会急剧下降，大部分时间浪费在了 as-if-serial 的语义维护上）
 *
 * @author 青石路
 * @date 2021/3/28 15:48
 */
public class OutOfOrderDemo {

    static int x = 0, y = 0;
    static int a = 0, b = 0;

    public static void main(String[] args) throws Exception {


        // 假设单线程下不会乱序，那么 x,y 的值就不会出现 (0, 0)
        // 而一旦 (x,y) 的值出现 (0, 0)，那么说说明单线程下存在乱序
        for (int i = 0; ; i++) {

            x = 0;
            y = 0;
            a = 0;
            b = 0;

            Thread t1 = new Thread(() -> {
                a = 1;
                x = b;
            });

            Thread t2 = new Thread(() -> {
                b = 1;
                y = a;
            });

            t1.start();
            t2.start();
            t1.join();
            t2.join();

            if (x == 0 && y == 0) {
                System.err.println("第" + i + "次出现 (" + x + "," + y + ")");
                break;
            }
        }
    }
}
