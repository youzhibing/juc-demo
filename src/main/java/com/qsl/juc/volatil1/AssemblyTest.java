package com.qsl.juc.volatil1;

/**
 * 有意思的问题：https://www.zhihu.com/question/263528143
 * https://www.cnblogs.com/stevenczp/p/7975776.html
 * https://www.cnblogs.com/stevenczp/p/7978554.html
 * <p>
 * binutils下载：http://mirror.us-midwest-1.nexcess.net/gnu/binutils/
 * openJDK下载：http://hg.openjdk.java.net/
 * 在64位Windows上编译hsdis：https://blog.csdn.net/yizishou/article/details/53423409
 * <p>
 * linux：hsdis-amd64.so 拷贝到 {JAVA_HOME}/jre/lib/amd64/server
 * win10：hsdis-amd64.dll拷贝到 {JAVA_HOME}/jre/bin/server（JDK 9以下）或JDK_HOME/lib/amd64/server（JDK 9或以上）
 * VM options 中配上 -XX:+UnlockDiagnosticVMOptions -XX:+PrintAssembly
 * -XX:+UnlockDiagnosticVMOptions -XX:+PrintAssembly -XX:+LogCompilation -XX:LogFile=jit.log
 * <p>
 * 打开 jitwatch：jitwatch的home目录下执行 mvn clean compile exec:java
 * <p>
 * JIT中有两种编译器，C1代表的是Client Compiler,C2代表的是Server Compiler。
 *
 * @author 青石路
 * @date 2021/2/23 20:29
 */
public class AssemblyTest {

    private static boolean stop = false;
    private static volatile int i = 0;

    public static void main(String[] args) throws Exception {

        while (!stop) {
            i++;
            if (i == 100000) {
                stop = true;
            }
        }
    }
}
