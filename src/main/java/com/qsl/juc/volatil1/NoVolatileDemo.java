package com.qsl.juc.volatil1;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @author 青石路
 * @date 2021/3/18 21:06
 */
public class NoVolatileDemo {

    static int num = 0;

    public static void main(String[] args) throws InterruptedException {

        // CountDownLatch latch = new CountDownLatch(1);
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "\t" + " num = " + num);
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            num = 6;
            // latch.countDown();
            System.out.println(Thread.currentThread().getName() + "\t" + " num = " + num);
        }, "t1").start();

        // latch.await();
        while (num == 0) {

        }
        System.out.println(Thread.currentThread().getName() + "\t" + " num = " + num);
    }

}
