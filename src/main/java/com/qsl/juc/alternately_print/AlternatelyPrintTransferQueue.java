package com.qsl.juc.alternately_print;

import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TransferQueue;

public class AlternatelyPrintTransferQueue {

    public static void main(String[] args) {

        char[] digitalArr = "1234567".toCharArray();
        char[] letterArr = "ABCDEFG".toCharArray();

        TransferQueue<Character> queue = new LinkedTransferQueue<>();

        new Thread(() -> {

            try {
                for (char digital : digitalArr) {
                    queue.transfer(digital);
                    System.out.print(queue.take());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }, "t1").start();

        new Thread(() -> {
            try {
                for (char letter : letterArr) {
                    System.out.print(queue.take());
                    queue.transfer(letter);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "t2").start();
    }
}
