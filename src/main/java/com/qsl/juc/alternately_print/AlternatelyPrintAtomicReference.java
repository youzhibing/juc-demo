package com.qsl.juc.alternately_print;

import java.util.concurrent.atomic.AtomicReference;

public class AlternatelyPrintAtomicReference {

    private static Thread t1 = null, t2 = null;

    public static void main(String[] args) {

        char[] digitalArr = "1234567".toCharArray();
        char[] letterArr = "ABCDEFG".toCharArray();

        AtomicReference<Thread> atomicReference = new AtomicReference<>();

        t1 = new Thread(() -> {
            for (char digital : digitalArr) {
                while (atomicReference.get() != t1) {
                }  // 自旋
                System.out.print(digital);
                // atomicReference.compareAndSet(t1, t2);
                atomicReference.set(t2);
            }
        }, "t1");

        t2 = new Thread(() -> {
            for (char letter : letterArr) {
                while (atomicReference.get() != t2) {
                }  // 自旋
                System.out.print(letter);
                // atomicReference.compareAndSet(t2, t1);
                atomicReference.set(t1);
            }
        }, "t2");

        atomicReference.set(t1);    // 从 t1 开始打印

        t1.start();
        t2.start();
    }
}
