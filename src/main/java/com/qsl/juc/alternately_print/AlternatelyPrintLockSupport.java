package com.qsl.juc.alternately_print;

import java.util.concurrent.locks.LockSupport;

/**
 * 交替打印 最优解
 * 线程同步中千万别用 sleep，因为 sleep 多少都不对
 */
public class AlternatelyPrintLockSupport {

    private static Thread t1 = null, t2 = null;

    public static void main(String[] args) {
        char[] digitalArr = "1234567".toCharArray();
        char[] letterArr = "ABCDEFG".toCharArray();

        // lock 与 unlock 的顺序是有先后的，先 lock 再 unlock
        // 但是 park 和 unpark 没有这个先后限制，可以先 unpark 再 park -- permit
        // 比如 t1 先调 LockSupport.unpart(t2), 然后 t2 再掉 LockSupport.part(), 那么它 t2 会 park 不住，继续执行

        t1 = new Thread(() -> {
            for (char digital : digitalArr) {
                System.out.print(digital);
                LockSupport.unpark(t2);         // 叫醒 t2
                LockSupport.park();             // t1 阻塞
            }
        }, "t1");

        t2 = new Thread(() -> {
            for (char letter : letterArr) {
                LockSupport.park();             // t2 阻塞
                System.out.print(letter);
                LockSupport.unpark(t1);         // 叫醒 t1
            }
        }, "t2");

        // t1.start()、t2.start() 能否交换位置，为什么
        t1.start();
        t2.start();
    }

}
