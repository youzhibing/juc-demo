package com.qsl.juc.alternately_print;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class AlternatelyPrintBlockingQueue {

    public static void main(String[] args) {

        char[] digitalArr = "1234567".toCharArray();
        char[] letterArr = "ABCDEFG".toCharArray();

        BlockingQueue<String> bq1 = new ArrayBlockingQueue<>(1);
        BlockingQueue<String> bq2 = new ArrayBlockingQueue<>(1);

        new Thread(() -> {
            for (char digital : digitalArr) {
                System.out.print(digital);
                try {
                    bq1.put("ok");      // 如果队列满了，一直阻塞，直到队列不满了或者线程被中断
                    bq2.take();             // 如果队列空了，一直阻塞，直到队列不为空或者线程被中断
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "t1").start();

        new Thread(() -> {
            for (char letter : letterArr) {
                try {
                    bq1.take();             // 如果队列满了，一直阻塞，直到队列不满了或者线程被中断
                    bq2.put("ok");      // 如果队列空了，一直阻塞，直到队列不为空或者线程被中断
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.print(letter);
            }
        }, "t2").start();
    }
}
