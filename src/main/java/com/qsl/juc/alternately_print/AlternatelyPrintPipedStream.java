package com.qsl.juc.alternately_print;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

/**
 * 利用管道的阻塞
 * 效率低
 */
public class AlternatelyPrintPipedStream {

    public static void main(String[] args) throws Exception {
        char[] digitalArr = "1234567".toCharArray();
        char[] letterArr = "ABCDEFG".toCharArray();

        PipedInputStream pis1 = new PipedInputStream();
        PipedInputStream pis2 = new PipedInputStream();
        PipedOutputStream pos1 = new PipedOutputStream();
        PipedOutputStream pos2 = new PipedOutputStream();

        pis1.connect(pos2);
        pis2.connect(pos1);

        String msg = "Your Turn";
        new Thread(() -> {
            byte[] buffer = new byte[9];
            try {
                for (char digital : digitalArr) {
                    System.out.print(digital);
                    pos1.write(msg.getBytes());
                    pis1.read(buffer);
                    if (new String(buffer).equals(msg)) {
                        continue;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }, "t1").start();

        new Thread(() -> {
            byte[] buffer = new byte[9];
            try {
                for (char letter : letterArr) {
                    pis2.read(buffer);              // 读一个字符
                    if (new String(buffer).equals(msg)) {
                        System.out.print(letter);
                    }
                    pos2.write(msg.getBytes());     // 写入
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }, "t2").start();
    }
}
