package com.qsl.juc.alternately_print;

public class AlternatelyPrintVolatile {

    static volatile boolean FLAG = true;

    public static void main(String[] args) {
        char[] digitalArr = "1234567".toCharArray();
        char[] letterArr = "ABCDEFG".toCharArray();

        new Thread(() -> {
            int i = 0;
            for (; i < digitalArr.length; ) {
                System.out.println("t1");
                if (FLAG) {
                    System.out.println(digitalArr[i]);
                    i++;
                    FLAG = false;
                }
            }
        }, "t1").start();

        new Thread(() -> {
            int i = 0;
            for (; i < letterArr.length; ) {
                System.out.println("t2");
                if (!FLAG) {
                    System.out.println(letterArr[i]);
                    i++;
                    FLAG = true;
                }
            }
        }, "t2").start();
    }
}
