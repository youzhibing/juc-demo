package com.qsl.juc.alternately_print;

public class AlternatelyPrintSynchronized {

    public static void main(String[] args) {
        final Object obj = new Object();
        char[] digitalArr = "1234567".toCharArray();
        char[] letterArr = "ABCDEFG".toCharArray();

        new Thread(() -> {
            synchronized (obj) {
                for (char digital : digitalArr) {
                    System.out.print(digital);
                    try {
                        // obj.notify()、obj.wait() 能否交换位置，为什么
                        obj.notify();
                        obj.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                // obj.notify() 能否去掉
                obj.notify();       // 避免有线程未被唤醒，否则无法停止程序
            }
        }, "t1").start();

        new Thread(() -> {
            synchronized (obj) {
                for (char letter : letterArr) {
                    System.out.print(letter);
                    try {
                        // obj.notify()、obj.wait() 能否交换位置，为什么
                        obj.notify();
                        obj.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                // obj.notify() 能否去掉
                obj.notify();       // 避免有线程未被唤醒，否则无法停止程序
            }
        }, "t2").start();
    }
}
