package com.qsl.juc.alternately_print;

import java.util.concurrent.atomic.AtomicInteger;

public class AlternatelyPrintAtomicInteger {

    private static AtomicInteger threadNo = new AtomicInteger(1);

    public static void main(String[] args) {
        char[] digitalArr = "1234567".toCharArray();
        char[] letterArr = "ABCDEFG".toCharArray();

        new Thread(() -> {
            for (char digital : digitalArr) {
                while (threadNo.get() != 1) {
                }  // 自旋
                System.out.print(digital);
                threadNo.set(2);
            }
        }, "t1").start();

        new Thread(() -> {
            for (char letter : letterArr) {
                while (threadNo.get() != 2) {
                }  // 自旋
                System.out.print(letter);
                threadNo.set(1);
            }
        }, "t2").start();
    }
}
