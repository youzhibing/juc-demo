package com.qsl.juc;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * @author 青石路
 * @date 2021/5/16 12:47
 */
public class LockSupportDemo {

    public static void main(String[] args) throws Exception {

        CountDownLatch latch = new CountDownLatch(1);
        Thread t = new Thread(() -> {
            latch.countDown();
            // LockSupport.park();
            LockSupport.park("blocker");
        }, "t1");
        t.start();

        latch.await();
        TimeUnit.SECONDS.sleep(30);
        System.out.println(Thread.currentThread().getName() + " 线程睡眠结束...");

        LockSupport.unpark(t);
    }
}
