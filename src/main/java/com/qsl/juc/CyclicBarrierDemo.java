package com.qsl.juc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo {

    public static void main(String[] args) {
        CyclicBarrier barrier = new CyclicBarrier(7, () -> System.out.println(Thread.currentThread().getName() + ", 龙珠集齐，召唤神龙"));

        for (int i = 1; i <= 7; i++) {
            new Thread(() -> {
                System.out.println("龙珠 " + Thread.currentThread().getName() + " 被找到");
                try {
                    barrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }, String.valueOf(i)).start();
        }

        System.out.println(Thread.currentThread().getName() + " 退出");
    }

}
