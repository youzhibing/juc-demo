package com.qsl.juc.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author 青石路
 * @date 2021/4/11 9:27
 */
public class ReentrantLockDemo {

    public static void main(String[] args) {

        // 独占锁
        ReentrantLock lock = new ReentrantLock();

        // 读写重入锁
        ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
        Lock readLock = readWriteLock.readLock();
        Lock writeLock = readWriteLock.writeLock();

        readLock.lock();

    }

}
