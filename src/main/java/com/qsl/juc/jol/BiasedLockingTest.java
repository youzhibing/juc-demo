package com.qsl.juc.jol;

import org.openjdk.jol.info.ClassLayout;

/**
 * //关闭延迟开启偏向锁
 * -XX:BiasedLockingStartupDelay=0
 * //禁止偏向锁
 * -XX:-UseBiasedLocking
 * //启用偏向锁
 * -XX:+UseBiasedLocking
 * <p>
 * java -X
 * java -XX:+PrintFlagsFinal  -version
 * <p>
 * BiasedLocking
 * new 对象的时候，若偏向锁已启动，则是匿名偏向，没启动则是普通对象
 */
public class BiasedLockingTest {

    public static void main(String[] args) throws Exception {
        // Thread.sleep(5000); // 偏向锁默认 4s 开启测试

        Object o = new Object();

        System.out.println(ClassLayout.parseInstance(o).toPrintable());

        synchronized (o) {
            System.out.println(ClassLayout.parseInstance(o).toPrintable());
        }
    }
}
