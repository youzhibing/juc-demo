package com.qsl.juc.jol;

import org.openjdk.jol.info.ClassLayout;

/**
 * @author 青石路
 * @date 2021/3/28 22:14
 */
public class ObjectJolDemo {

    public static void main(String[] args) {
        // objectTest();
        // customClassTest();
        booleanByteTest();
    }


    /**
     * new Object() 占多少个字节
     */
    public static void objectTest() {
        Object o = new Object();
        System.out.println(ClassLayout.parseInstance(o).toPrintable());
    }

    /**
     * 自定义类的对象占多少个字节，与属性值有关
     */
    public static void customClassTest() {
        T t = new T();
        System.out.println(ClassLayout.parseInstance(t).toPrintable());
    }

    public static void booleanByteTest() {
        B b = new B();
        System.out.println(ClassLayout.parseInstance(b).toPrintable());
    }

    static class T {
        int m = 8;
    }

    static class B {
        // boolean b = true;
        boolean[] arr = {true, false, true, true, true};
    }
}
