package com.qsl.juc;

/**
 * @author 青石路
 * @date 2021/3/7 18:15
 */
public class ThreadLocalTest {

    ThreadLocal<Long> longThreadLocal = new ThreadLocal<>();

    public static void main(String[] args) {

        ThreadLocalTest test = new ThreadLocalTest();

        new Thread(() -> {
            test.longThreadLocal.set(0L);
            for (long i = 0; i < 10; i++) {
                test.longThreadLocal.set(test.longThreadLocal.get() + i);
            }
            System.out.println(Thread.currentThread().getName() + " : " + test.longThreadLocal.get());
        }, "T1").start();

        new Thread(() -> {
            test.longThreadLocal.set(0L);
            for (long i = 100; i < 110; i++) {
                test.longThreadLocal.set(test.longThreadLocal.get() + i);
            }
            System.out.println(Thread.currentThread().getName() + " : " + test.longThreadLocal.get());
        }, "T2").start();

    }
}
