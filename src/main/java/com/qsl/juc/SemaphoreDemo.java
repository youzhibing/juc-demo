package com.qsl.juc;

import org.junit.Test;
import sun.misc.Unsafe;

import java.util.concurrent.Semaphore;

public class SemaphoreDemo {

    @Test
    public void normal() {
        Semaphore semaphore = new Semaphore(3);

        for (int i = 1; i <= 6; i++) {
            new Thread(() -> {
                try {
                    semaphore.acquire();
                    System.out.println("车" + Thread.currentThread().getName() + " 入库成功");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release();
                    System.out.println("车" + Thread.currentThread().getName() + " 出库成功");
                }
            }, String.valueOf(i)).start();
        }
    }

    @Test
    public void zero() throws InterruptedException {
        Semaphore semaphore = new Semaphore(0);
        semaphore.acquire();
        System.out.println("end...");
    }
}
