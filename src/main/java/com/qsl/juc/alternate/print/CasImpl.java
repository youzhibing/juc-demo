package com.qsl.juc.alternate.print;

public class CasImpl {

    private static volatile boolean flag = true;

    public static void main(String[] args) {
        char[] digitArr = "1234567".toCharArray();
        char[] strArr = "ABCDEFG".toCharArray();

        new Thread(() -> {
            for (char digit : digitArr) {
                while (!flag) {
                }        // 自旋
                System.out.print(digit);
                flag = false;
            }
        }).start();

        new Thread(() -> {
            for (char str : strArr) {
                while (flag) {
                }         // 自旋
                System.out.print(str);
                flag = true;
            }
        }).start();
    }
}
