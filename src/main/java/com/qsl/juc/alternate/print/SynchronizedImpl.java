package com.qsl.juc.alternate.print;

public class SynchronizedImpl {

    public static void main(String[] args) {
        Object obj = new Object();
        char[] digitArr = "1234567".toCharArray();
        char[] strArr = "ABCDEFG".toCharArray();

        new Thread(() -> {
            synchronized (obj) {
                for (char digit : digitArr) {
                    System.out.print(digit);
                    try {
                        obj.notify();
                        obj.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                obj.notify();
            }
        }).start();

        new Thread(() -> {
            synchronized (obj) {
                for (char str : strArr) {
                    System.out.print(str);
                    try {
                        obj.notify();
                        obj.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                obj.notify();
            }
        }).start();

    }
}
