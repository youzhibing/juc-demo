package com.qsl.juc.alternate.print;

import java.util.concurrent.locks.LockSupport;

public class LockSupportImpl {

    private static Thread digitThread = null, strThread = null;

    public static void main(String[] args) {
        char[] digitArr = "1234567".toCharArray();
        char[] strArr = "ABCDEFG".toCharArray();

        digitThread = new Thread(() -> {
            for (char digit : digitArr) {
                System.out.print(digit);
                LockSupport.unpark(strThread);
                LockSupport.park();
            }
        });
        digitThread.start();

        strThread = new Thread(() -> {
            for (char str : strArr) {
                LockSupport.park();
                System.out.print(str);
                LockSupport.unpark(digitThread);
            }
        });
        strThread.start();
    }
}
