package com.qsl.test;

/**
 * @author 青石路
 * @date 2021/3/18 19:16
 */
public class IntTest {

    public static void main(String[] args) {
        int i = 0;
        System.out.println("MIN_VALUE = " + Integer.MIN_VALUE + ", MAX_VALUE = " + Integer.MAX_VALUE);
        while (true) {
            if (i < 0) {
                System.out.println("i = " + i);
                break;
            }
            i++;
        }
    }
}
