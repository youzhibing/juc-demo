package com.qsl.test;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * 单向链表反转
 * @author 青石路
 * @date 2021/4/9 20:45
 */
public class ReverseListTest {

    public static void main(String[] args) {
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;

        //Node reversed = reverse(node1);
        Node reversed = reversePlus(node1);
        printList(reversed);
    }

    /**
     * 这种实现效率比较高
     *      迭代实现（其实还可以递归实现）
     * @param h
     * @return
     */
    public static Node reversePlus(Node h) {
        Node newHead = null, oldNext = null;
        while (h != null) {
            oldNext = h.next;
            h.next = newHead;
            newHead = h;
            h = oldNext;
        }
        return newHead;
    }

    public static Node reverse(Node list) {

        if (list == null || list.next == null) {
            return list;
        }

        Node pre = list, tail = pre.next;
        Node newHead = null;

        while (pre.next != null) {

            // 找到尾节点（tail） 和 尾节点的前节点（pre）
            while(tail.next != null) {
                pre = tail;
                tail = pre.next;
            }
            if (newHead == null) {
                // 设置新的头结点
                newHead = tail;
            }

            // 尾节点与其前节点 角色互换
            pre.next = null;
            tail.next = pre;

            // 重置 pre、tail 到旧链表上
            if (list.next != null) {
                pre = list;
                tail = pre.next;
            }
        }
        return newHead;
    }

    public static void printList(Node node) {
        if (node == null) {
            return;
        }
        while (node != null) {
            System.out.print(node.data + " ");
            node = node.next;
        }
    }

    public static void reverseApi() {
        List<Integer> list = new LinkedList();
        list.add(1);
        list.add(2);
        list.add(3);
        System.out.println(list);
        Collections.reverse(list);
        System.out.println(list);
    }

    static class Node {
        int data;
        Node next;

        Node (int data) {
            this.data = data;
        }
    }
}
